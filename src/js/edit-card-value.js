// Константы блока формы редактирования карточки
const editCardBlock = $('.edit-card-block')
const closeEditBtn = $('.wrapper__close-edit-block')
const editCardBtn = $('.edit-card-form__btn')
const editSelectDoctor = $('#edit-select-doctor')
console.log(editSelectDoctor)

// событие на закрытие формы
closeEditBtn.on('click', () => {
    editCardBlock.toggle('slow')
    $('.edit-card-form__value').val(null)
    $('.edit-card-form__therapist').hide('slow')
    $('.edit-card-form__cardiologist').hide('slow')
    $('.edit-card-form__dentist').hide('slow')
})
// событие на добавление дополнительных свойств для каждого врача
editSelectDoctor.on('blur', () => {
    if(editSelectDoctor.val() === 'Терапевт'){
        $('.edit-card-form__therapist').show('slow')
        $('.edit-card-form__dentist').hide('slow')
        $('.edit-card-form__cardiologist').hide('slow')
    }
    if(editSelectDoctor.val() === 'Стоматолог'){
        $('.edit-card-form__dentist').show('slow')
        $('.edit-card-form__therapist').hide('slow')
        $('.edit-card-form__cardiologist').hide('slow')
    }
    if(editSelectDoctor.val() === 'Кардиолог'){
        $('.edit-card-form__cardiologist').show('slow')
        $('.edit-card-form__therapist').hide('slow')
        $('.edit-card-form__dentist').hide('slow')
    }

})

// Функции создания обьекта для разных врачей , классы беруться с файла create-visit.js
function editTherapist() {
    let newVisit = new visitTherapist (
       `${$('#edit-select-doctor').val()}` ,
       `${$('#edit-visit-target').val()}` ,
       `${$('#edit-visit-description').val()}` ,
       `${$('#edit-urgency-visit').val()}` ,
       `${$('#edit-fullName').val()}` ,
       `${$('#edit-age-therapist').val()}`

    )
    
    return newVisit
}

function editDentist(){
    let newVisit = new VisitDentist (
        `${$('#edit-select-doctor').val()}`,
       `${$('#edit-visit-target').val()}`,
       `${$('#edit-visit-description').val()}`,
       `${$('#edit-urgency-visit').val()}`,
       `${$('#edit-fullName').val()}`,
       `${$('#edit-age-therapist').val()}`,
        `${$('#edit-lastDataVisit').val()}`
 )
 
    
    return newVisit
 }

 function editCardiologist() {
    let newVisit = new VisitCardiologist (
        `${$('#edit-select-doctor').val()}` ,
        `${$('#edit-visit-target').val()}` ,
        `${$('#edit-visit-description').val()}` ,
        `${$('#edit-urgency-visit').val()}` ,
        `${$('#edit-fullName').val()}` ,
        `${$('#edit-age-therapist').val()}`,
        `${$('#edit-pressure').val()}`,
        `${$('#edit-index-body').val()}`,
        `${$('#edit-diseasesOfCVS').val()}`,
        `${$('#edit-age-cardiologist').val()}`
        )
    return newVisit
 }
// константы для проверки на наличие значений в инпутах . erroeFormValue - функция берет аргументом 
// HTML-коллекцию и делает проверку на наличие значений
const editInputVal = document.getElementsByClassName('edit-card-form__value')
const editInputValTherapist = document.getElementsByClassName('edit-card-form__value-therapist')
const editInputValDentist = document.getElementsByClassName('edit-card-form__value-dentist')
const editInputValCardiologist = document.getElementsByClassName('edit-card-form__value-cardiologist')

// функция , которая делает ПУТ запрос с удалением карточки со страницы и добавляет отредактированую 

 async function putEditCard(id, obj){
   let res = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${TOKEN}`
  },
  body: JSON.stringify(obj)
})
  .then(response => response.json())
  .then(obj => {
    document.getElementById(`${obj.id}-wrapper`).remove()
     return getVisitCard(obj)
  })
  return res
}

// функция , которая отправляет пут запрос с готовым обьектом и закрывает форму
 async function editCardValue(){
   
    let id = document.querySelector('.wrapper__edit-title').innerHTML.slice(21)
    
    if(errorFormValue(editInputVal) === true && errorFormValue(editInputValTherapist) === true && editSelectDoctor.val() === 'Терапевт'){
       let visit = await editTherapist()
     await  putEditCard(id, visit)
      editCardBlock.toggle('slow')
    }
   
       
    
    if(errorFormValue(editInputVal) === true && errorFormValue(editInputValDentist) === true && editSelectDoctor.val() === "Стоматолог"){
        let visit =  editDentist()
        await putEditCard(id, visit)
        editCardBlock.toggle('slow')
    }

    if(errorFormValue(editInputVal) === true && errorFormValue(editInputValCardiologist) === true && editSelectDoctor.val() === 'Кардиолог'){
        let visit = editCardiologist()
         await putEditCard(id, visit)
        editCardBlock.toggle('slow')
    }
  
}

editCardBtn.on('click', editCardValue)
