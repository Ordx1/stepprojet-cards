const btnCreateeVisit = $('#btn__createVisit'); // Кнопка для подвязки "Создать визит"
const visitCard = document.querySelector('.filter-block__visit-cards')// Это блок , куда будут подгружаться созданые визиты
const mainWrapper = $('.create-visit-block__wrapper') // Карточка визита , являеться дочерним єлементом visitCard
const TOKEN = 'fcc84d1f-1c22-4fd0-87d3-6b2508e6378d' // Токен , необходим для работы с сервером

// событие , которое при нажатии на кнопку "Создать визит" показывает форму для создания визита
btnCreateeVisit.on('click', () =>{
    $('.create-visit-block').toggle('slow')
    btnCreateeVisit.toggle('slow')
    
})
// Функция , которая при закрытии формы очищает значения в инпутах
function clearFormValue(){
    $('.main_item').val(null)
    $('.main_item-dentist').val(null)
    $('.main_item-therapist').val(null)
    $('.main_item-cardiologist').val(null)
    $('.value-therapist').attr('display' , 'none')
    $('.create-visit-block').toggle('slow')
    $('.value-dentist').attr('style', 'display:none')
    $('.value-cardiologist').attr('style', 'display:none')
    $('.value-therapist').attr('style', 'display:none')
    btnCreateeVisit.toggle('slow')
}
$('.wrapper__close-btn').on('click', clearFormValue)

// При выборе доктора в форме создании визита - подгружает дополнительные значения для каждого врача
const selectDoctor = $('#select-doctor')
selectDoctor.on('blur', () => {
    if(selectDoctor.val() === ""){
        $('.value-dentist').hide('slow')
        $('.value-cardiologist').hide('slow')
        $('.value-therapist').hide('slow')
    }
    if (selectDoctor.val() === "Терапевт") {
        $('.value-dentist').hide('slow')
      $('.value-cardiologist').hide('slow')
      $('.value-therapist').show('slow')
    } 
     if(selectDoctor.val() === "Стоматолог"){
        $('.value-dentist').show('slow')
      $('.value-cardiologist').hide('slow')
      $('.value-therapist').hide('slow')
     
    }

    if (selectDoctor.val() === "Кардиолог"){
        $('.value-dentist').hide('slow')
        $('.value-therapist').hide('slow')
        $('.value-cardiologist').show('slow')
       
    }  
})


class visit {
    constructor(doctor, visitTarget, description, urgency ,fullName){
        this.doctor = doctor ;
        this.visitTarget = visitTarget ;
        this.description = description ;
        this.urgency = urgency ;
        this.fullName = fullName ;
    }
}

class visitTherapist extends visit {
    constructor(doctor, visitTarget, description, urgency ,fullName , age){
        super(doctor, visitTarget, description, urgency ,fullName)
        this.age = age ;
    }
}

class VisitDentist extends visit {
    constructor(doctor, visitTarget, description, urgency ,fullName , lastDataVisit){
        super(doctor, visitTarget, description, urgency ,fullName)
        this.lastDataVisit = lastDataVisit;
    }
}

class VisitCardiologist extends visit {
    constructor(doctor, visitTarget, description, urgency ,fullName , 
        normalPressure , bodyWeightIndex , diseasesOfCVS , age) {
           super(doctor, visitTarget, description, urgency ,fullName)
            this.normalPressure = normalPressure;
            this.bodyWeightIndex = bodyWeightIndex;
            this.diseasesOfCVS = diseasesOfCVS;
            this.age = age;
        }
}

// функция , которая отправляет ПОСТ запрос и выводит карточку на экран
async function postToHost(data){
   await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${TOKEN}`
        },
        body: JSON.stringify(data)
      })
        .then(response => response.json())
        .then(obj => {
            getVisitCard(obj)
            getFiltrderCard()
        })
        
     }
// Функция , которая преобразует обьект в карточку и выводит ее на страницу
     function getVisitCard(obj){
        let diwVisible = document.createElement('div')
        let divHide = document.createElement('div')
        divHide.style.display = 'none'
        let divWrapper = document.createElement('div')
        divWrapper.classList.add('visit-cards__item')
        divWrapper.id = `${obj.id}-wrapper`
        diwVisible.classList.add('item__visible-block')
        diwVisible.classList.add('item__no-visible-block')
        let showBtn = document.createElement('button')
        let hideBtn = document.createElement('button')
        hideBtn.classList.add('item__bottom-btn', 'hide-card-btn')
        hideBtn.style.display ='none'
        showBtn.innerText = 'Показать больше' ;
        let editBtn = document.createElement('button')
        editBtn.classList.add('item__bottom-btn')
        editBtn.id = `edit-${obj.id}`
        
        editBtn.addEventListener('click', (event) =>{
            editCardBlock.toggle('slow')
    let fullId = event.target.id
    let id = fullId.slice(5)
    document.querySelector('.wrapper__edit-title').innerHTML = `Изменение карточки № ${id}`
        })
        showBtn.classList.add('item__bottom-btn', 'show-card-btn')
        editBtn.innerText ='Отредактировать'
        showBtn.addEventListener('click' , () => {
            hideBtn.style.display = 'inline-block'
            showBtn.style.display = 'none'
            divHide.style.display = 'block'
         })
    
         
         hideBtn.classList.add('item__bottom-btn', 'hide-card-btn')
         hideBtn.style.display ='none'
         hideBtn.addEventListener('click' , () => {
            hideBtn.style.display = 'none'
            showBtn.style.display = 'inline-block'
            divHide.style.display = 'none'
        
         })
         let deleteCardBtn = document.createElement('button')
         deleteCardBtn.innerHTML =`&#10006;`
         deleteCardBtn.style.float = 'right'
         deleteCardBtn.id = obj.id
         deleteCardBtn.addEventListener('click', deleteCard)
         hideBtn.innerText = 'Cвернуть карточку'
        for(let key in obj){
        
            let p = document.createElement('p')
        p.classList.add('item__list')
            if (key === 'doctor' || key === 'fullName'){
                p.innerHTML = `${key} : ${obj[key]}`
                diwVisible.append(p)
            } else {
                p.innerHTML = `${key} : ${obj[key]}`
                divHide.append(p)
            }
           }
    
    divWrapper.append(deleteCardBtn, diwVisible, divHide , hideBtn, showBtn, editBtn )
    visitCard.append(divWrapper)
    }
     
// Ниже три функции , которые создают обьект на основе классов созданых выше. Значения берутся с инпутов формы создания визита.
 function Therapist() {
    let newVisit = new visitTherapist (
       `${$('#select-doctor').val()}` ,
       `${$('#visit-target').val()}` ,
       `${$('#visit-description').val()}` ,
       `${$('#urgency-visit').val()}` ,
       `${$('#fullName').val()}` ,
       `${$('#age-therapist').val()}`

    )
    return newVisit
}

function Dentist(){
    let newVisit = new VisitDentist (
        `${$('#select-doctor').val()}` ,
        `${$('#visit-target').val()}` ,
        `${$('#visit-description').val()}` ,
        `${$('#urgency-visit').val()}` ,
        `${$('#fullName').val()}` ,
        `${$('#last-visit').val()}`
 
     )
 
    
    return newVisit
 }

 function Cardiologist() {
    let newVisit = new VisitCardiologist (
        `${$('#select-doctor').val()}` ,
        `${$('#visit-target').val()}` ,
        `${$('#visit-description').val()}` ,
        `${$('#urgency-visit').val()}` ,
        `${$('#fullName').val()}` ,
        `${$('#pressure').val()}`,
        `${$('#index-body').val()}`,
        `${$('#diseasesOfCVS').val()}`,
        `${$('#cardiologistA').val()}`
        )
    return newVisit
 }
 
// функция для проверки наличия значений в инпутах.Аргументом выступает HTML-коллекция инпутов
  function errorFormValue(elem){
      for (let i = 0 ; i <= elem.length - 1; i++){
        if (elem[i].value === "" ){
            $('.error-empty-value').text('Данные формы не заполнены!')
            return false
        } else {
           
            continue 
        }
    }
    $('.error-empty-value').text(null)
      return true 
      
      }
      // HTML- коллекции инпутов для проверки на наличие значений
      const mainItem = document.getElementsByClassName('main_item')
      const mainItemDentist = document.getElementsByClassName('main_item-dentist')
      const mainItemTherapist = document.getElementsByClassName('main_item-therapist')
      const mainItemCardiologist = document.getElementsByClassName('main_item-cardiologist')


      
// Эта функция собирает ранее созданые функции: Делает проверку на наличие значний , создает обьект и отправляет его на сервер,
// После чего карточка появляется в браузере , значения в инпутах очищаются и модальное окно закрывается . В случае если на странице
// не было карточек , и вместо них была надпись "No items have been added" - этому тексту присваивается класс "hide-block", который
// скрывает его.
 async function createVisit () {
   if ($('#select-doctor').val() === "Терапевт"  && errorFormValue(mainItem) === true && errorFormValue(mainItemTherapist) === true ){
            let therapist =  Therapist()
            await postToHost(therapist)
            clearFormValue() 
          return document.querySelector('.main_noItems').classList.add('hide-block')
          }
         if ($('#select-doctor').val() === "Стоматолог" &&  errorFormValue(mainItem) === true && errorFormValue(mainItemDentist) === true ){
            let dentist =  Dentist()
            await postToHost(dentist)
            clearFormValue() 
           return document.querySelector('.main_noItems').classList.add('hide-block')
          }
        if ($('#select-doctor').val() === "Кардиолог" && errorFormValue(mainItem) === true && errorFormValue(mainItemCardiologist) === true){
            let cardiologist =  Cardiologist()
            await postToHost(cardiologist)
            clearFormValue() 
           return document.querySelector('.main_noItems').classList.add('hide-block')
           }
    }
   
const wrapperBottomBtn = $('.wrapper__bottom-btn')
wrapperBottomBtn.on('click', createVisit)