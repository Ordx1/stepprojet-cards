const doctorInput = document.querySelector('#filter-visit')
const urgencyInput = document.querySelector('#urg-visit')

// функция которая выгружает все созданые ранее визити на экран. В конце используется функция "getVisitCard" с документа create-visit.js
async function getAllVisitCard () {
  await fetch(`https://ajax.test-danit.com/api/v2/cards`,{
      method: 'GET',
      headers: {
         'Content-Type': 'application/json',
          'Authorization': `Bearer ${TOKEN}`
           },
       })
.then(response => response.json())
.then(response => {
  let cards = [...response]
   if (cards.length === 0){
    document.querySelector('.main_noItems').classList.remove('hide-block')
     }else {
      document.querySelector('.main_noItems').classList.add('hide-block')
      }
      cards.forEach(obj => {
       getVisitCard(obj)
        })
           
      
       })
}
getAllVisitCard()
// Добавил новую функцию для фильтрации . Пусть getAllVisitCard() будет для первоначальной выгрузки всех карточке , 
// функциия ниже - для их сортировки 
 function getFiltrderCard(){
 
 fetch(`https://ajax.test-danit.com/api/v2/cards`,{
    method: 'GET',
     headers: {
      'Content-Type': 'application/json',
       'Authorization': `Bearer ${TOKEN}`
          },
      })
    .then(response => response.json())
    .then(response => {
      let cards = [...response]
      cards.forEach(obj => {
        if(doctorInput.value === '' &  urgencyInput.value === ''){
         return document.getElementById(`${obj.id}-wrapper`).classList.remove('hide-block')
       }
        if (obj.doctor === doctorInput.value && urgencyInput.value === obj.urgency){
         return document.getElementById(`${obj.id}-wrapper`).classList.remove('hide-block')
          } 
      if(obj.doctor === doctorInput.value &&  urgencyInput.value === ''){
        document.getElementById(`${obj.id}-wrapper`).classList.remove('hide-block')
        } else if (urgencyInput.value === obj.urgency && doctorInput.value === ''){
          document.getElementById(`${obj.id}-wrapper`).classList.remove('hide-block')
           } else {
              return document.getElementById(`${obj.id}-wrapper`).classList.add('hide-block')
             }
         
})
          })
  }
 

$('#filter-visit').on('change', getFiltrderCard)
$('#urg-visit').on('change', getFiltrderCard)

  // функция на удаление карточки. Удаляет созданную карточку как с сервера так и с экрана ,
  // и в случае отсутствия карточек выводит текс "No items have been added". Функция присваиватся при создании карточки
  // кнопке в функции "getVisitCard" с документа create-visit.js
function  deleteCard(event) {
   let id = event.target.id
   let acceptUser = confirm('Вы точно хотите удалить карточку?')
  if (acceptUser === true){
       fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
 method: 'DELETE',
 headers: {
   'Authorization': `Bearer ${TOKEN}`
 },
})
document.getElementById(`${id}-wrapper`).remove()
 } if (document.querySelector('.filter-block__visit-cards').children.length === 0) {
  document.querySelector('.main_noItems').classList.remove('hide-block')
 }
}

