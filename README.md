### Состав участников проекта:
1. Ростислав Ожигов ;
2. Хижняк Андрей;
3. Дидов Владислав .

### Какие задачи выполняли участники:
1. Ростислав Ожигов выполнил работу по стилизации сайта и модального окна авторизации.
2. Хижняк Андрей выполнил работу по фильтрации  карточек,  их редактирование и удаление.
3. Дидов Владислав выполнил работу по модальному окну "Создать визит" и выгрузки карточек на страницу.

